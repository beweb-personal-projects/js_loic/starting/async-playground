window.onload = () => {
    console.log("sync1");

    let multiText = (cb) => {
        const data = {
            name: "strackz",
            level: 30,
            admin: true
        }
        cb(data);
    }
    
    console.log("sync2");

    multiText((data) => {
        setTimeout(() => {
            console.log(data);
            console.log("-------------------------------------------------");
        }, 300);
    })

    console.log("sync3");

    // getAllPlayersPromise();
    getAllPlayersAsync();

    console.log("sync4");

    function add(verif, cb, ...args) {
        if(verif) {
            let sum = 0;
            args.forEach(num => sum += num);
            cb(sum);
        }
    }

    add(true, (value) => {
        console.log("Value: " + Number(value));
    }, 30, 20, 50)
}

function getPlayers() {
    const persons =  [
        {name: "hi1", admin: false},
        {name: "hi2", admin: true},
        {name: "hi3", admin: false},
    ]

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            (persons.length > 0) ? resolve(persons.length) : reject();
        }, 500)
    })
}

function numberAdded() {
    let number = 50;
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            (number > 0) ? resolve(number) : reject("No Number Added");
        }, 500)
    })
}

// function getAllPlayersPromise() {
//     getPlayers().then((persons) => {
//         numberAdded().then((number) => {
//             console.log(persons * number);
//         }).catch(() => {
//             console.log("No Number Added");
//         })
//     }).catch(() => {
//         console.log("No people inside the table");
//     })
// }

async function getAllPlayersAsync() {
    try {
        let number_final = await numberAdded() * await getPlayers();
        console.log(number_final);
    } catch {
        console.log("No people inside the table");
    }
}

