window.onload = () => {
    // ------------------------------------------------------------------------------------------------ //
    // CALLBACK SYSTEM
    // ------------------------------------------------------------------------------------------------ //
    function getPlayersCB(cb) {
        const persons =  [
            {name: "hi1", admin: false},
            {name: "hi2", admin: true},
            {name: "hi3", admin: false},
        ]
        cb(persons);
    }

    function retrievePlayersCB() {
        getPlayersCB((data) => {
            console.log(data);
        })
    }

    retrievePlayersCB();

    // ------------------------------------------------------------------------------------------------ //
    // PROMISES SYSTEM
    // ------------------------------------------------------------------------------------------------ //

    function getPlayersPromise() {
        const persons =  [
            {name: "hi1", admin: false},
            {name: "hi2", admin: true},
            {name: "hi3", admin: false},
        ]

        return new Promise((resolve, reject) => {
            if(persons.length > 0) {
                resolve(persons);
            } else {
                reject();
            }
        })
    }

    function retrievePlayersPromise() {
        getPlayersPromise().then((persons) => {
            persons.forEach(e => {
                console.log(e);
            });
        }).catch(() => {
            console.log("No data inside the array of Promise");
        })
    }

    retrievePlayersPromise();

    // ------------------------------------------------------------------------------------------------ //
    // ASYNC / AWAIT SYSTEM
    // ------------------------------------------------------------------------------------------------ //

    async function retrievePlayersAsync() {
        try {
            let persons = await getPlayersPromise();
            persons.forEach(e => {
                console.log(e);
            });
        } catch {
            console.log("No data inside the array of Async/Await");
        }

    }

    retrievePlayersAsync();

    // ------------------------------------------------------------------------------------------------ //
    // TESTING ZONE
    // ------------------------------------------------------------------------------------------------ //

    let arrayTesting = ["a", "b", "c"];

    arrayTesting.unshift("startUnshift");
    arrayTesting = ["start...", ...arrayTesting];

    arrayTesting = [...arrayTesting, "end..."];
    arrayTesting.push("endPush");

    console.log(arrayTesting);
}